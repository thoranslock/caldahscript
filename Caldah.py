import maya.cmds as cmds
import json
import functools
import os

class Caldah:
    def __init__(self, *args):
        print "WADDUP"
        # MAKE WINDOW
        self.windowWidth = 400
        self.windowHeight = 260
        self.previewSize = 260
        self.counter = 0
        self.windowID = "Caldah"
        self.proj_dir = "{}Caldah/".format(cmds.internalVar(userWorkspaceDir=True))
        self.setups_dir = self.proj_dir+'presets/'

        caldahImage = self.proj_dir + '/images/Caldah.png'
        titleImage = self.proj_dir + '/images/Title.png'

        print caldahImage

        print self.windowWidth, self.windowHeight

        if cmds.window(self.windowID, exists=True):  # simple line deletes the previous window
            cmds.deleteUI(self.windowID)
        self.counter += 1
        self.win = cmds.window(self.windowID, height=self.windowHeight, width=self.windowWidth,
                               title=self.windowID, sizeable=False)

        # Make Title
        cmds.columnLayout(adj=True)
        cmds.rowLayout(numberOfColumns=2)
        cmds.image(image=caldahImage, width=50, height=50)
        cmds.image(image=titleImage, width=350, height=50)
        cmds.setParent('..')

        cmds.separator(height=10, style='none')

        cmds.rowLayout(numberOfColumns=4)
        cmds.separator(width=5, style='none')
        cmds.text(label="Select Preset", font="boldLabelFont")
        cmds.separator(width=5, style='none')
        cmds.optionMenu(changeCommand=self.load_preset, w=300)

        for roots, dirs, files in os.walk(self.setups_dir):
            for file in files:
                if file.endswith('.txt'):
                    baseName = file.replace(".txt", "")
                    cmds.menuItem(label=baseName)
        cmds.setParent('..')

        cmds.separator(height=20, style='none')

        cmds.text(label="-- Add Preset --", font="boldLabelFont", w=self.windowWidth)

        cmds.separator(height=10, style='none')

        cmds.rowLayout(numberOfColumns=4)
        cmds.separator(width=5, style='none')
        cmds.text(label="Width:", width=60, align='left', font="boldLabelFont")
        cmds.separator(width=5, style='none')
        self.customWidth = cmds.intField(width=300)
        cmds.setParent('..')

        cmds.separator(height=10, style='none')

        cmds.rowLayout(numberOfColumns=4)
        cmds.separator(width=5, style='none')
        cmds.text(label="Height:", width=60, align='left', font="boldLabelFont")
        cmds.separator(width=5, style='none')
        self.customHeight = cmds.intField(width=300)
        cmds.setParent('..')

        cmds.separator(height=10, style='none')

        cmds.rowLayout(numberOfColumns=4)
        cmds.separator(width=5, style='none')
        cmds.text(label="Name:", width=60, align='left', font="boldLabelFont")
        cmds.separator(width=5, style='none')
        self.customName = cmds.textField(text="CustomPreset", width=300)
        cmds.setParent('..')

        cmds.separator(height=10, style='none')

        cmds.rowLayout(numberOfColumns=6)
        cmds.separator(width=5, style='none')

        cmds.button(label="Save Custom Setup", w=380, command=functools.partial(
            self.write_preset))
        cmds.separator(width=5, style='none')
        cmds.setParent('..')

    def write_preset(self, *args):
        width = cmds.intField(self.customWidth, q=1, value=True)
        height = cmds.intField(self.customHeight, q=1, value=True)
        name = cmds.textField(self.customName, q=1, text=True)

        file_name = name+'.txt'
        new_file = os.path.join(self.setups_dir, file_name)

        save_dic = {"width": width, "height": height}
        jSonString = json.dumps(save_dic)

        fileHandle = open(new_file, "w")
        fileHandle.write(jSonString)
        fileHandle.close()
        print "Saved into %s (test)" % file_name

    def load_preset(self, preset, *args):
        file_name = preset+".txt"
        load_file = os.path.join(self.setups_dir, file_name)

        if os.path.exists(load_file):
            file_handle = open(load_file, "r")
            file_string = file_handle.read()
            self.load_dic = json.loads(file_string)

            width = self.load_dic["width"]
            height = self.load_dic["height"]

            cmds.setAttr("defaultResolution.width", width)
            cmds.setAttr("defaultResolution.height", height)
            cmds.setAttr("defaultResolution.deviceAspectRatio", (width / height))
            cmds.setAttr("defaultResolution.lockDeviceAspectRatio", 0)
            cmds.setAttr("defaultResolution.pixelAspect", 1.0)


    def showUI(self, *args):
        print "UI RUN (test)"
        cmds.showWindow(self.win)
        # gMainWindow = maya.mel.eval('$tmpVar=$gMainWindow')
        cmds.window(self.win, edit=True, widthHeight=(self.windowWidth, self.windowHeight))


script = Caldah()
script.showUI()
